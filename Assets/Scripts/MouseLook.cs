﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public Transform playerBody;

    float xRotation = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        // follow mouse y with the camera
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -65f, 60f);  // to avoid looking too high or too low

        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);

        // rotate the player with respect to mouse x
        playerBody.Rotate(Vector3.up * mouseX);

    }
}

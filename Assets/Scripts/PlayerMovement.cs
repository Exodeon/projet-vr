﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 0.25f;
    public float gravity = -0.25f;
    public static bool isSlashing = false;
    public static int score = 0;
    public Text scoreText;
    public Text livesText;
    public Text defeatText;
    public float attackLength = 1f;
    public float invulnerabilityTime = 1f;

    private Animator anim;	
    private int lives = 5;
    private float lastAttackTime = 0f;  
    private float lastHitTime = 0f; 
    Vector3 velocity;

    void Start ()
	{
		anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Score : " + score;
        livesText.text = "Vies : " + lives;

        // hit when player clicks
        if (Input.GetMouseButtonUp(0))
        {
            anim.SetBool("Slash", true);
            isSlashing = true;
            lastAttackTime = Time.time;
            GetComponent<AudioSource>().Play();
        } else {
            anim.SetBool("Slash", false);
        }

        if (Time.time > lastAttackTime + attackLength) { // attack animation finished
            isSlashing = false;
        }

        if (controller.isGrounded && velocity.y < 0) {
            velocity.y = 0f;
        } else {
            velocity.y += gravity * Time.deltaTime;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

		anim.SetFloat("Speed", z);	

        if (z < 0) {    // to go slowly backward
            z = z / 2;
        }

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move((move * speed + velocity) * Time.deltaTime);
    }

    void OnTriggerStay(Collider other) {
        // if we get hit by the enemy's saber
        if (other.gameObject.CompareTag("EnemySaber") && EnemyController.isSlashing && (Time.time > lastHitTime + invulnerabilityTime))
        {
            lives--;
            lastHitTime = Time.time;
            if (lives <= 0) {
                defeatText.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
        }
    }
}

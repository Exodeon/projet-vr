﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class R2D2Controller : MonoBehaviour
{
    public float maxDistance = 20;

    private UnityEngine.AI.NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!agent.hasPath) {
            Vector3 randomPos = Random.insideUnitSphere * maxDistance + transform.position;

            UnityEngine.AI.NavMeshHit hit;
            // from randomPos find a nearest point on NavMesh surface in range of maxDistance
            UnityEngine.AI.NavMesh.SamplePosition(randomPos, out hit, maxDistance, UnityEngine.AI.NavMesh.AllAreas);
            agent.SetDestination(hit.position);
        }
    }
}

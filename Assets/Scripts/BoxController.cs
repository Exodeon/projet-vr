﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour
{
	public float timeLeft = 5.0f;
    void Start()
    {
        // Make the crate disappear gradually
         iTween.FadeTo(gameObject, iTween.Hash("alpha", 0f, "time", timeLeft, "onComplete", "Disappear"));
    }

    void Disappear()
    {
        Destroy(gameObject);
    }
}

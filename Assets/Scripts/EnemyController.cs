﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public Transform playerTransform;
    public UnityEngine.AI.NavMeshAgent agent;
    public float attackCooldown = 2.0f;
    public AudioSource saberSource;
    public static bool isSlashing = false;
    public float invulnerabilityTime = 1f;

    private int lives = 3;
    private Animator anim;	
    private float lastAttackTime = 0f;	// store the last time we attacked, for cooldown
    private bool inFrontOfPlayer = false;
    private float lastHitTime = 0f; // store the last time we get hit, to apply invulnerability
    private bool scoreGiven = false;    // indicates if the score has been increased after death


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("Slash", false);
        anim.SetBool("Dead", false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!anim.GetBool("Dead") && !inFrontOfPlayer) {	
            agent.SetDestination(playerTransform.position);
            anim.SetFloat("Speed", agent.speed);
        } 
        else {
            if (anim.GetBool("Dead")) {
                GetComponent<Rigidbody>().Sleep();
                GetComponent<AudioSource>().Stop();
            }
            agent.ResetPath();
            anim.SetFloat("Speed", 0f);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!anim.GetBool("Dead")) {
            if (other.CompareTag("Player")) {
                inFrontOfPlayer = true;
            }

            // if something is in the trigger (player, wooden box, R2D2), we hit
            if (LayerMask.LayerToName(other.gameObject.layer) != "Ground" && !other.CompareTag("EnemySaber")) {
                if (Time.time > lastAttackTime + attackCooldown) {
                    anim.SetBool("Slash", true);
                    lastAttackTime = Time.time;
                    isSlashing = true;
                    saberSource.PlayDelayed(0.5f);
                } else if (Time.time > lastAttackTime + 1.5) {  // attack animation finished
                    anim.SetBool("Slash", false);
                    isSlashing = false;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player")) {
            inFrontOfPlayer = false;
        }
        anim.SetBool("Slash", false);
    }

    void OnCollisionEnter(Collision other) {
        // if we get hit by the player's saber
        if (other.gameObject.CompareTag("PlayerSaber") && PlayerMovement.isSlashing && (Time.time > lastHitTime + invulnerabilityTime))
        {
            lives--;
            lastHitTime = Time.time;
            if (lives <= 0) {
                anim.SetBool("Dead", true);
                if (!scoreGiven) {
                    scoreGiven = true;
                    PlayerMovement.score += 1000;
                    isSlashing = false;
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform headTransform;

    // Update is called once per frame
    void Update()
    {
        // Place the camera on the empty GameObject in front of the head
        GetComponent<Transform>().position = new Vector3(headTransform.position.x, GetComponent<Transform>().position.y, headTransform.position.z);
    }
}
